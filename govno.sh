curl --request POST \
      --header "PRIVATE-TOKEN: $MY_TOKEN" \
      --form "variables[][key]=REGISTRY_URL" --form "variables[][value]=$REGISTRY_URL" \
      --form "variables[][key]=REGISTRY_URL_N" --form "variables[][value]=$REGISTRY_URL_N" \
      "https://gitlab.com/api/v4/projects/27307726/pipeline?ref=main" | jq . ; \
PIPI_ID=$(curl --header "PRIVATE-TOKEN: $MY_TOKEN" "https://gitlab.com/api/v4/projects/27307726/pipelines/" | jq '.[] | select (.status =="skipped") .id')
JOB_ID=$(curl -k --location --header "PRIVATE-TOKEN: $MY_TOKEN " "https://gitlab.com/api/v4/projects/27307726/pipelines/$PIPI_ID/jobs" | jq '.[] | select (.name =="run") .id')
curl -k --request POST --header "PRIVATE-TOKEN: $MY_TOKEN" "https://gitlab.com/api/v4/projects/27307726/jobs/$JOB_ID/play"
